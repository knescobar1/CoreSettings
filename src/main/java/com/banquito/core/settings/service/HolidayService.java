package com.banquito.core.settings.service;

import com.banquito.core.settings.exception.HolidayException;
import com.banquito.core.settings.dao.HolidayRepository;
import com.banquito.core.settings.model.Holiday;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class HolidayService {
  private final HolidayRepository holidayRepository;

  public HolidayService(HolidayRepository holidayRepository) {
    this.holidayRepository = holidayRepository;
  }

  public Holiday findById(Date date) {

    Optional<Holiday> holidayOpt = this.holidayRepository.findById(date);
    if (holidayOpt.isPresent()) {
      return holidayOpt.get();
    } else {
      return null;
    }
  }

  public List<Holiday> betweenDatesRegionNull(Date from, Date to) {
    return this.holidayRepository.findByDateBetweenAndRegionIdIsNull(from, to);
  }

  public List<Holiday> betweenDatesRegionNotNull(Date from, Date to, String region) {
    return this.holidayRepository.findByDateBetweenAndRegionId(from, to, region);
  }

  public Holiday create(Holiday holiday) throws HolidayException {
    Holiday holidayDb = this.findById(holiday.getDate());
    if (holidayDb != null) {
      throw new HolidayException("Holiday already exists");
    }
    holiday.setCreationDate(new Date());
    return this.holidayRepository.save(holiday);
  }

  public void generateWeekendsAsHoliday(Date from, Date to) {
    Calendar start = Calendar.getInstance();
    start.setTime(from);
    Calendar end = Calendar.getInstance();
    end.setTime(to);
    for (Date date = start.getTime();
        start.before(end);
        start.add(Calendar.DATE, 1), date = start.getTime()) {
      if (start.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
          || start.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
        Holiday holiday = new Holiday();
        holiday.setDate(date);
        holiday.setCreationDate(new Date());
        if (start.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
          holiday.setName(
              "Saturday, "
                  + start.get(Calendar.DAY_OF_MONTH)
                  + "/"
                  + start.get(Calendar.MONTH)
                  + "/"
                  + start.get(Calendar.YEAR));
        }
        if (start.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
          holiday.setName(
              "Sunday, "
                  + start.get(Calendar.DAY_OF_MONTH)
                  + "/"
                  + start.get(Calendar.MONTH)
                  + "/"
                  + start.get(Calendar.YEAR));
        }
        holiday.setRegionId(null);
        this.holidayRepository.save(holiday);
      }
    }
  }

  public Date obtainNextBusinessDay(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    Holiday holidayDb;
    do {
      do {
        holidayDb = this.findById(calendar.getTime());
        if (holidayDb != null) {
          calendar.add(Calendar.DATE, 1);
        }
      } while (holidayDb != null);
      if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
        calendar.add(Calendar.DATE, 2);
      } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
        calendar.add(Calendar.DATE, 1);
      }
      holidayDb = this.findById(calendar.getTime());
      if (holidayDb != null) {
        calendar.add(Calendar.DATE, 1);
      }
    } while (holidayDb != null);
    return calendar.getTime();
  }

  public Integer obtainNumberOfDaysOfNextBusinessDay(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    Holiday holidayDb;
    int numberOfHolidayDays = 0;
    do {
      do {
        holidayDb = this.findById(calendar.getTime());
        if (holidayDb != null) {
          calendar.add(Calendar.DATE, 1);
          numberOfHolidayDays++;
        }
      } while (holidayDb != null);
      if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
        calendar.add(Calendar.DATE, 2);
        numberOfHolidayDays += 2;
      } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
        calendar.add(Calendar.DATE, 1);
        numberOfHolidayDays++;
      }
      holidayDb = this.findById(calendar.getTime());
      if (holidayDb != null) {
        calendar.add(Calendar.DATE, 1);
        numberOfHolidayDays++;
      }
    } while (holidayDb != null);
    return numberOfHolidayDays;
  }

  public void delete(Date date) throws HolidayException {
    Holiday holidayDb = this.findById(date);
    Date dateNow = new Date();
    if (holidayDb == null) {
      throw new HolidayException("Holiday does not exist");
    }
    if (date.before(dateNow)) {
      throw new HolidayException("Holiday already passed");
    }
    this.holidayRepository.delete(holidayDb);
  }
}
