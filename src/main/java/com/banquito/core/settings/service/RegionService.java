package com.banquito.core.settings.service;

import com.banquito.core.settings.dao.RegionRepository;
import com.banquito.core.settings.model.Region;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RegionService {
    private final RegionRepository regionRepository;

    public RegionService(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    public Region findById(String id) {
        Optional<Region> regionOpt = this.regionRepository.findById(id);
        if (regionOpt.isEmpty()) {
            return null;
        }
        return regionOpt.get();
    }


}
