package com.banquito.core.settings.service;

import com.banquito.core.settings.dao.BankInformationRepository;
import com.banquito.core.settings.dao.CurrencyRepository;
import com.banquito.core.settings.exception.BankInformationException;
import com.banquito.core.settings.exception.CurrencyNotFoundException;
import com.banquito.core.settings.model.BankInformation;
import com.banquito.core.settings.model.Currency;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

@Service
public class BankInformationService {
    private final BankInformationRepository bankInformationRepository;
    private final CurrencyRepository currencyRepository;

    public BankInformationService(BankInformationRepository bankInformationRepository, CurrencyRepository currencyRepository) {
        this.bankInformationRepository = bankInformationRepository;
        this.currencyRepository = currencyRepository;
    }

    public BankInformation createBank(BankInformation bankInf) throws BankInformationException {
        Optional<BankInformation> bankOpt = this.bankInformationRepository.findById(bankInf.getBankCode());
        if (bankOpt.isPresent()) {
            throw new BankInformationException("Record already exist");
        }
        Optional<Currency> currencyOpt = this.currencyRepository.findById(bankInf.getCurrency());
        if (currencyOpt.isEmpty()) {
            throw new CurrencyNotFoundException("Currency not found");
        }
        bankInf.setCreationDate(new Date());
        bankInf.setTimeZoneId(String.valueOf(ZonedDateTime.now()));

        return this.bankInformationRepository.save(bankInf);
    }

    public BankInformation modifyBankInformation(BankInformation bankInf) throws BankInformationException {
        Optional<BankInformation> bankInfOpt = this.bankInformationRepository.findById(bankInf.getBankCode());
        if (bankInfOpt.isEmpty()) {
            throw new BankInformationException("Record not found");
        }
        Optional<Currency> currencyOpt = this.currencyRepository.findById(bankInf.getCurrency());
        if (currencyOpt.isEmpty()) {
            throw new CurrencyNotFoundException("Currency not found");
        }
        BankInformation bankInfDB = bankInfOpt.get();
        bankInfDB.setName(bankInf.getName());
        bankInfDB.setCountry(bankInf.getCountry());
        bankInfDB.setCurrency(bankInf.getCurrency());
        bankInfDB.setCity(bankInf.getCity());
        bankInfDB.setPostalCode(bankInf.getPostalCode());
        bankInfDB.setWebSite(bankInf.getWebSite());
        bankInfDB.setEmail(bankInf.getEmail());
        bankInfDB.setPhoneNumber(bankInf.getPhoneNumber());
        return this.bankInformationRepository.save(bankInfDB);
    }

    public BankInformation loadInformation(String bankCode) {
        Optional<BankInformation> bankInfOpt = this.bankInformationRepository.findById(bankCode);
        if (bankInfOpt.isEmpty()) {
            return null;
        }
        return bankInfOpt.get();
    }
}
