package com.banquito.core.settings.resource;

import com.banquito.core.settings.dto.BankInformationDTO;
import com.banquito.core.settings.model.BankInformation;
import com.banquito.core.settings.service.BankInformationService;
import lombok.RequiredArgsConstructor;
import net.bytebuddy.asm.Advice;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/bankInf")
public class BankInformationResource {

    private final BankInformationService bankInfService;

    @GetMapping(path = "/{bankCode}")
    public ResponseEntity<BankInformationDTO> getBankInformation(@PathVariable String bankCode){
        try {
            return ResponseEntity.ok(this.buildBankInfDTO(bankInfService.loadInformation(bankCode)));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping
    public ResponseEntity<BankInformationDTO> createBankInfomation(@RequestBody BankInformationDTO bankInfDTO){
        try{
            BankInformation bankInformation = this.bankInfService.createBank(this.buildBankInformation(bankInfDTO));
            return ResponseEntity.ok(this.buildBankInfDTO(bankInformation));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping
    public ResponseEntity<BankInformationDTO> modifyBankInformation(@RequestBody BankInformationDTO bankInfDTO){
        try{
            BankInformation bankInformation = this.bankInfService.modifyBankInformation(this.buildBankInformation(bankInfDTO));
            return ResponseEntity.ok(this.buildBankInfDTO(bankInformation));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }

    }

    private BankInformation buildBankInformation(BankInformationDTO bankInf){
        return BankInformation.builder()
                .bankCode(bankInf.getBankCode())
                .name(bankInf.getName())
                .country(bankInf.getCountry())
                .currency(bankInf.getCurrency())
                .city(bankInf.getCity())
                .postalCode(bankInf.getPostalCode())
                .webSite(bankInf.getWebSite())
                .email(bankInf.getEmail())
                .phoneNumber(bankInf.getPhoneNumber())
                .fiscalId(bankInf.getFiscalId())
                .build();

    }

    private BankInformationDTO buildBankInfDTO(BankInformation bankInf){
        return BankInformationDTO.builder()
                .bankCode(bankInf.getBankCode())
                .name(bankInf.getName())
                .country(bankInf.getCountry())
                .currency(bankInf.getCurrency())
                .city(bankInf.getCity())
                .postalCode(bankInf.getPostalCode())
                .webSite(bankInf.getWebSite())
                .email(bankInf.getEmail())
                .phoneNumber(bankInf.getPhoneNumber())
                .fiscalId(bankInf.getFiscalId())
                .build();
    }
}
