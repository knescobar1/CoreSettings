package com.banquito.core.settings.resource;

import com.banquito.core.settings.dto.BranchDTO;
import com.banquito.core.settings.model.Branch;
import com.banquito.core.settings.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/branch")
public class BranchResource {
    private final BranchService branchService;

    @GetMapping(path = "/{id}")
    public ResponseEntity<Branch> searchById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(this.branchService.getByBranchId(id));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping(path = "/region/{id}")
    public ResponseEntity<List<Branch>> searchByRegion(@PathVariable String id){
        try{
            return ResponseEntity.ok(this.branchService.getByRegion(id));
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }
    @PostMapping
    public ResponseEntity<BranchDTO> create(BranchDTO dto){
        try {
            Branch branch = this.branchService.create(this.buildBranch(dto));
            return ResponseEntity.ok(this.buildBranchDTO(branch));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping
    public ResponseEntity<BranchDTO> update (BranchDTO dto){
        try {
            Branch branch = this.branchService.update(this.buildBranch(dto));
            return ResponseEntity.ok(this.buildBranchDTO(branch));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    private Branch buildBranch(BranchDTO dto){
        return Branch.builder()
                .id(dto.getId())
                .code(dto.getCode())
                .name(dto.getName())
                .addressCountry(dto.getAddressCountry())
                .addressCity(dto.getAddressCity())
                .addressPostalCode(dto.getAddressPostalCode())
                .addressLine1(dto.getAddressLine1())
                .addressLine2(dto.getAddressLine2())
                .phoneNumber(dto.getPhoneNumber())
                .locationUrl(dto.getLocationUrl())
                .email(dto.getEmail())
                .regionId(dto.getRegionId()).build();
    }
    private BranchDTO buildBranchDTO( Branch branch){
        return BranchDTO.builder()
                .id(branch.getId())
                .code(branch.getCode())
                .name(branch.getName())
                .addressCountry(branch.getAddressCountry())
                .addressCity(branch.getAddressCity())
                .addressPostalCode(branch.getAddressPostalCode())
                .addressLine1(branch.getAddressLine1())
                .addressLine2(branch.getAddressLine2())
                .phoneNumber(branch.getPhoneNumber())
                .locationUrl(branch.getLocationUrl())
                .email(branch.getEmail())
                .regionId(branch.getRegionId()).build();
    }

}
