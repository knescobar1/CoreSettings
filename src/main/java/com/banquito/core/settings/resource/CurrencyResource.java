package com.banquito.core.settings.resource;

import com.banquito.core.settings.dto.CurrencyDTO;
import com.banquito.core.settings.model.Currency;
import com.banquito.core.settings.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/currency")
public class CurrencyResource {

  private final CurrencyService currencyService;

  @GetMapping(path = "/{id}")
  public ResponseEntity<CurrencyDTO> searchById(@PathVariable String id) {
    try {
      return ResponseEntity.ok(this.buildCurrencyDTO(this.currencyService.findById(id)));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PostMapping
  public ResponseEntity<CurrencyDTO> create(@RequestBody CurrencyDTO dto) {
    Currency currency = this.currencyService.create(this.buildCurrency(dto));
    return ResponseEntity.ok(this.buildCurrencyDTO(currency));
  }

  @PutMapping
  public ResponseEntity<CurrencyDTO> update(@RequestBody CurrencyDTO dto) {
    try {
      Currency currency = this.currencyService.update(this.buildCurrency(dto));
      return ResponseEntity.ok(this.buildCurrencyDTO(currency));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @DeleteMapping(path = "/{id}")
  public ResponseEntity delete(@PathVariable String id) {
    try {
      this.currencyService.delete(id);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  private Currency buildCurrency(CurrencyDTO dto) {
    return Currency.builder().id(dto.getId()).name(dto.getName()).build();
  }

  private CurrencyDTO buildCurrencyDTO(Currency currency) {
    return CurrencyDTO.builder().id(currency.getId()).name(currency.getName()).build();
  }
}
