package com.banquito.core.settings.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BankInformationException extends Exception{
    public BankInformationException(String message) {
        super(message);
    }
}
