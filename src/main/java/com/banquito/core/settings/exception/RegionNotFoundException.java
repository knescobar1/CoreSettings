package com.banquito.core.settings.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RegionNotFoundException extends Exception {

  public RegionNotFoundException(String message) {
    super(message);
  }
}
