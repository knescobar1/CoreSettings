package com.banquito.core.settings.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CurrencyDTO {

    private String id;

    private String name;
}
