package com.banquito.core.settings.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "BNK_CURRENCY")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Currency implements Serializable {

  private static final long serialVersionUID = 277854628L;

  @Id
  @Column(name = "CURRENCY_ID", length = 3, nullable = false)
  @NonNull
  @EqualsAndHashCode.Include
  private String id;

  @Column(name = "NAME", length = 32, nullable = false)
  private String name;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CREATION_DATE", nullable = false)
  private Date creationDate;
}
