package com.banquito.core.settings.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "BNK_BRANCH")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Branch implements Serializable {

  private static final long serialVersionUID = 48525541551628L;

  @Id
  @Column(name = "BRANCH_ID", length = 32, nullable = false)
  @NonNull
  @EqualsAndHashCode.Include
  private String id;

  @Column(name = "CODE", length = 8, nullable = false)
  private String code;

  @Column(name = "NAME", length = 64, nullable = false)
  private String name;

  @Column(name = "ADDRESS_COUNTRY", length = 3, nullable = false)
  private String addressCountry;

  @Column(name = "ADDRESS_CITY", length = 64, nullable = false)
  private String addressCity;

  @Column(name = "ADDRESS_POSTAL_CODE", length = 8)
  private String addressPostalCode;

  @Column(name = "ADDRESS_LINE1", length = 128, nullable = false)
  private String addressLine1;

  @Column(name = "ADDRESS_LINE2", length = 128, nullable = false)
  private String addressLine2;

  @Column(name = "PHONE_NUMBER", length = 15, nullable = false)
  private String phoneNumber;

  @Column(name = "LOCATION_URL", length = 256, nullable = false)
  private String locationUrl;

  @Column(name = "EMAIL", length = 128, nullable = false)
  private String email;

  @Column(name = "REGION_ID", length = 32)
  private String regionId;
}
