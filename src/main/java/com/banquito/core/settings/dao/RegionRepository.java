package com.banquito.core.settings.dao;

import com.banquito.core.settings.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepository extends JpaRepository<Region, String> {}
