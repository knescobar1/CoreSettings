package com.banquito.core.settings.dao;

import com.banquito.core.settings.model.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface HolidayRepository extends JpaRepository<Holiday, Date> {

  List<Holiday> findByDateBetweenAndRegionIdIsNull(Date from, Date to);

  List<Holiday> findByDateBetweenAndRegionId(Date from, Date to, String region);
}
